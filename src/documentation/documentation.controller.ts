import { Controller, Req, Param, Body, Query } from '@nestjs/common';
import { Get } from '@nestjs/common';
import { Request } from 'express';
@Controller('api/documentation')
export class DocumentationController {
  @Get('query')
  QueryDecorator(@Query() queryObject) {
    console.log('Example: /api/documentation/query?a=1&b=2&c=3');
    return queryObject;
  }
  @Get('body')
  BodyDecorator(@Body() bodyObject) {
    return bodyObject;
  }
  @Get('request')
  ReqDecorator(@Req() reqObject: Request) {
    console.log(reqObject);
    return { route: reqObject.route.path, body: reqObject.body };
  }
  @Get(':param')
  ParamDecorator(@Param() paramObject) {
    return paramObject;
  }
}
