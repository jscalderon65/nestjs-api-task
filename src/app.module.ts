import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TaskModule } from './task/task.module';
import { DocumentationModule } from './documentation/documentation.module';

@Module({
  imports: [TaskModule, DocumentationModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
