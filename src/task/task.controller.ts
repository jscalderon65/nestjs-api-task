import { Controller, Req, Param } from '@nestjs/common';
import { Post /* , Get, Put, Patch, Delete */ } from '@nestjs/common';
import { Request } from 'express';

@Controller('api/v1/task')
export class TaskController {
  @Post(':id')
  method(@Req() req: Request, @Param('id') param) {
    return {
      Param: param,
      Request: req.method,
    };
  }
}
